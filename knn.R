
# You need to install these packages beforehand.
# Example: install.packages("caret")
library(caret) # Machine learning
library(dplyr) # Data manipulation 
library(doParallel)


# Configuration
set.seed(1)
cl <- makeCluster(detectCores() - 1)
registerDoParallel(cl)

# Read data.
dataset <- read.csv("hr.csv", stringsAsFactors = F)


# Dataset has already been cleaned.


# Feature selection (manually).
dataset <- dplyr::select(dataset, -Work_accident, -sales)
outcomeVar <- "left"  # Set dependent variable (outcomeVar to predict).

# Change outcome variable type, from numbers to Y or N
dataset[, outcomeVar] <- as.factor(ifelse(dataset[, outcomeVar], "Y", "N"))
table(dataset$left)
prop.table(table(dataset$left)) * 100


# Randomly split dataset into training (70%) and testing (30%)
inTraining <- caret::createDataPartition(dataset[, outcomeVar], p = 0.7, list = F)
training <- dataset[inTraining,]
testing  <- dataset[-inTraining,]

# Create train control object with all the desired configuration parameters.
trControl <- trainControl(
  method = "repeatedcv",  # Use cross validation to find the best K.
  repeats = 2, # Repeat the cross validation procedure 20 times.
  classProbs = TRUE, # Output class probabilities instead of the assigned class.
  verboseIter = F # Do not output all the training process.
)

preProcess <- c("scale", "center") # Define the pre processing steps you want, in this case: scale and center 
                                   # so our numeric variables are in the same scale

# This is the same as:  left ~ satisfaction_level + last_evaluation + number_project + average_montly_hours 
#                              + time_spend_company + salary + promotion_last_5years 
customFormula <- left ~ . 

# Train the model.
model <- train(
  customFormula, # The first parameter is always the formula to define the outcome and predictors.
  data = training, # Training dataset
  method = "knn", # Algorithm to use
  preProcess = preProcess, # Pre processing object, already defined.
  trControl = trControl, # Training recipe.
  tuneLength = 3 # How many different k values to try.
)

# Predict the outcome of the testing dataset using the new model
testingPred <- predict(model, newdata = testing)

# Calculate metrics to evaluate our model. 
# This will compare our predictions (testingPred) with the real outcome (testing$left)
confusionMatrix(testingPred, testing$left, positive = "Y")

# Another example to predict
example <- data.frame(
  satisfaction_level = 0.3,
  last_evaluation = 0.43,
  number_project = 7,
  average_montly_hours = 300,
  time_spend_company = 2,
  promotion_last_5years = 0,
  salary = "low"
)

# Get probability of leaving the company
predict(model, newdata = example, type = "prob")$Y * 100
